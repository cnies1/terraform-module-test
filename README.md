Terraform Module Test
=====================
This test is to show how terraform modules are scoped.

This "master" branch is the start setup.  We have two child modules in terraform, child1 and child2, that are independent from each other.

Run the following commands:
```
terraform init
```
```
terraform apply
```
The output will be:
```
child_1_output = "This is child 1"
child_2_output = "This is child 2"
```
Next checkout the branch "step1":
`git checkout step1`