output "child_1_output" {
  description = "Text of child1"
  value = module.the_child_1.text
}

output "child_2_output" {
  description = "Text of child2"
  value = module.the_child_2.text
}